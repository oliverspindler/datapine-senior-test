/*
    Routes all requests that don't match static files to index.html.
    This allows deep links to non-index Backbone routes.
*/

module.exports = function () {
    'use strict';

    var staticDirMatcher = /^\/(bower_components|fixtures|scripts|styles)/;

    return function speclistGenerator (req, res, next) {
        if (!req.url.match(staticDirMatcher)) {
            req.url = '/';
        }

        return next();
    };
};
