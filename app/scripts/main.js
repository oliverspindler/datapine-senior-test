/*global require*/
'use strict';

require.config({
    shim: {
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: [
                'underscore',
                'jquery'
            ],
            exports: 'Backbone'
        }
    },
    paths: {
        backbone: '../bower_components/backbone/backbone',
        d3: '../bower_components/d3/d3',
        jquery: '../bower_components/jquery/jquery',
        underscore: '../bower_components/underscore/underscore',
        text: '../bower_components/requirejs-text/text',
        tpl: '../bower_components/requirejs-tpl-jfparadis/tpl'
    },
    tpl: {
        path: 'templates/'
    }
});
