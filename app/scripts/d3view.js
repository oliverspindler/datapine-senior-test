/* global define */
/* jshint expr: true */

define([
    'view',
    'd3'
], function (View, d3) {
    'use strict';

    var D3View = View.extend({

        render: function () {
            View.prototype.render.apply(this, arguments);

            this.$el.toggleClass('thumbnail', !!this.thumbnail);
            this.$el.toggleClass('fullsize', !this.thumbnail);

            var innerEl = this.innerEl = $('<div class="inner"></div>');
            this.$el.append(innerEl);
            var margin = this.margin = {
                top: parseFloat(innerEl.css('top')),
                right: parseFloat(innerEl.css('right')),
                bottom: parseFloat(innerEl.css('bottom')),
                left: parseFloat(innerEl.css('left'))
            };
            this.width = this.$el.width() - margin.left - margin.right,
            this.height = this.$el.height() - margin.top - margin.bottom;
        },

        d3: d3
    });

    return D3View;
});
