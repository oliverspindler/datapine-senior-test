/* global define */

define([
    'underscore',
    'd3view',
    'tpl!cluster_dendrogram'
], function (_, D3View, template) {
    'use strict';

    var ClusterDendogramView = D3View.extend({

        template: template,

        className: 'cluster_dendrogram',

        events: {
            'change input': 'changeLayout'
        },

        render: function () {
            // based on http://bl.ocks.org/mbostock/4063570
            D3View.prototype.render.apply(this, arguments);

            var d3 = this.d3,
                width = this.width,
                height = this.height;

            var radius = this.radius = Math.min(width, height) / 2;

            var cluster = this.cluster = d3.layout.cluster()
                .size([radius * 0.75, radius * 0.75]);

            var svg = this.svg = d3.select(this.$el[0]).append('svg')
                .attr('width', width)
                .attr('height', height)
              .append('g')
                .attr('class', 'wrapper');

            d3.json('/fixtures/flare.json', _.bind(function(error, root) {
                var nodes = cluster.nodes(root),
                    links = cluster.links(nodes);

                svg.selectAll('.link')
                    .data(links)
                    .enter().append('path')
                    .attr('class', 'link');

                var node = svg.selectAll('.node')
                    .data(nodes)
                    .enter().append('g')
                    .attr('class', 'node');

                node.append('circle')
                    .attr('r', 4.5);

                node.append('text')
                    .attr('dx', function(d) { return d.children ? -8 : 8; })
                    .attr('dy', 3)
                    .style('text-anchor', function(d) { return d.children ? 'end' : 'start'; })
                    .text(function(d) { return d.name; });

                this.setCartesianLayout();
            }, this));
        },

        changeLayout: function (e) {
            var layout = $(e.target).val();
            if (layout === 'radial') {
                this.setRadialLayout(true);
            } else {
                this.setCartesianLayout(true);
            }
        },

        setCartesianLayout: function (animate) {
            var d3 = this.d3;
            var diagonal = d3.svg.diagonal()
                .projection(function(d) { return [d.y * 2, d.x * 4]; });
            var link = this.svg.selectAll('path.link');
            var svg = this.svg;
            var node = this.svg.selectAll('.node');

            if (animate) {
                link = link.transition().duration(500);
                svg = svg.transition().duration(500);
                node = node.transition().duration(500);
            }

            link.attr('d', diagonal);
            svg.attr('transform', 'translate(40,0)');
            node.attr('transform', function(d) { return 'translate(' + d.y * 2 + ',' + d.x * 4 + ')'; });

        },

        setRadialLayout: function (animate) {
            var d3 = this.d3;
            var diagonal = d3.svg.diagonal.radial()
                .projection(function(d) { return [d.y, d.x / 180 * Math.PI]; });
            var link = this.svg.selectAll('path.link');
            var svg = this.svg;
            var node = this.svg.selectAll('.node');

            if (animate) {
                link = link.transition().duration(500);
                svg = svg.transition().duration(500);
                node = node.transition().duration(500);
            }

            link.attr('d', diagonal);
            var offset = this.radius;
            svg.attr('transform', 'translate(' + offset + ',' + offset + ')');
            node.attr('transform', function(d) { return 'rotate(' + (d.x - 90) + ')translate(' + d.y+ ')'; });
        }
    });

    return ClusterDendogramView;
});
