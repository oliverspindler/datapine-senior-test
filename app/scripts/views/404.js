/* global define */

define([
    'view',
    'tpl!404'
], function (View, template) {
    'use strict';

    var Error404View = View.extend({
        template: template
    });

    return Error404View;
});
