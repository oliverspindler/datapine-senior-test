/*
Regenerates list of all spec files when speclist.js is requested
*/

var recursive = require('recursive-readdir');
var fs = require('fs');

module.exports = function () {
    return function speclistGenerator (req, res, next) {
        if (req.url !== '/speclist.js') return next();

        recursive('test/spec', function (err, files) {
            files = files.map(function(path) {
                var matches = path.match(/^test(.*)$/);
                return matches ? matches[1] : '';
            });

            var out = "window.specs = " + JSON.stringify(files);
            fs.writeFile(".tmp/speclist.js", out, function(err) {
                return next();
            });
        });
    }
}
