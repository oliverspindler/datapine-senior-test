/* global define */

define([
    'view',
    'tpl!navigation'
], function (View, template) {
    'use strict';

    var NavigationView = View.extend({
        template: template
    });

    return NavigationView;
});
