# datapine frontend test #

## Development setup

- Install `bower`: `npm install -g bower`
- Install `grunt-cli`: `npm install -g grunt-cli`
- Install Node.js dependencies: `npm install`
- Install bower dependencies: `bower install`

## Running the application

Type `grunt server` to run a development server on port 9000.

Visit the [about page](http://localhost:9000/about) for implementation details.

## Running tests

- `grunt connect:test:keepalive` to start a Mocha server on port 9001.
- `grunt test` to run specs via PhantomJS.
