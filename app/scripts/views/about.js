/* global define */

define([
    'view',
    'tpl!about'
], function (View, template) {
    'use strict';

    var AboutView = View.extend({
        template: template
    });

    return AboutView;
});
