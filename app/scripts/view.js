/* global define */

define([
    'underscore',
    'backbone'
], function (_, Backbone) {
    'use strict';

    var View = Backbone.View.extend({

        initialize: function(options) {
            Backbone.View.prototype.initialize.apply(this.arguments);
            _.extend(this, options);
        },

        events: {
            'click a[data-no-history!=true]': 'linkHandler',
        },

        render: function(options) {
            if (!options) {
                options = {};
            }
            if (this.template) {
                var context = _.extend(this.templateContext(), options.context);
                this.$el.html(this.template(context));
            }
        },

        templateContext: function() {
            var context;
            context = {
                model: this.model,
                collection: this.collection
            };
            if (this.model) {
                _.extend(context, this.model.toJSON());
            }
            return context;
        },

        linkHandler: function(e) {
            if (this.modifierPressed(e)) {
                return;
            }
            e.preventDefault();
            var href = $(e.currentTarget).attr('href');
            if (href && href.match(/https?:\/\//)) {
                return window.open(href);
            } else {
                return Backbone.history.navigate(href, true);
            }
        },

        modifierPressed: function(evt) {
            return _.some(['shift', 'meta', 'alt', 'ctrl'], function(key) {
                return evt[key + 'Key'];
            });
        }
    });

    return View;
});
