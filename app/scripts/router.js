/* global define */

define([
    'underscore',
    'backbone',
    'views/navigation',
    'views/index',
    'views/about',
    'views/404'
], function (_, Backbone, NavigationView, IndexView, AboutView, Error404View) {
    'use strict';

    var Router = Backbone.Router.extend({

        initialize: function () {
            Backbone.history.start({ pushState: true });
            var navigationView = new NavigationView({ el: $('#navigation') });
            navigationView.render();
        },

        routes: {
            '': 'index',
            'about(/)': 'about',
            ':id(/)': 'detail',
            '*notFound': 'notFound'
        },

        index: function() {
            this.setLayout(IndexView);
        },

        about: function() {
            this.setLayout(AboutView);
        },

        detail: function(id) {
            require(['views/' + id], _.bind(function (View) {
                this.setLayout(View);
            }, this), _.bind(function () {
                this.setLayout(Error404View);
            }, this));
        },

        notFound: function() {
            this.setLayout(Error404View);
        },

        setLayout: function(LayoutView) {
            if (this.layout) {
                this.layout.undelegateEvents();
                this.layout.remove();
            }
            this.layout = new LayoutView();
            $('#content').append(this.layout.$el);
            this.layout.render();
        }

    });

    return Router;
});
