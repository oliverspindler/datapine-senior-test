/* global define, describe, it, expect, sinon, beforeEach, afterEach */
/* jshint expr: true */

define([
    'view',
    'backbone'
], function (View, Backbone){
    'use strict';

    describe('View', function () {

        it('inherits from Backbone.View', function () {
            var view = new View();
            expect(view instanceof Backbone.View).to.be.true;
        });

        describe('render', function() {
            var view;
            view = null;
            beforeEach(function() {
                view = new View();
                sinon.stub(view, 'templateContext').returns({
                    a: 'b',
                    foo: 'bar'
                });
            });

            it('does nothing when no template is defined', function() {
                view.render();
                expect(view.$el.html()).to.equal('');
            });

            it('renders template with default context', function() {
                view.template = function(context) {
                    return context.a + ': ' + context.foo;
                };
                view.render();
                expect(view.$el.html()).to.equal('b: bar');
            });

            it('renders template with custom context', function() {
                view.template = function(context) {
                    return context.a + ': ' + context.foo;
                };
                view.render({
                    context: {
                        foo: 'baz'
                    }
                });
                expect(view.$el.html()).to.equal('b: baz');
            });
        });

        describe('templateContext', function() {
            it('provides model attributes as properties', function() {
                var context, model, view;
                model = new Backbone.Model({
                    a: 'b',
                    foo: 'bar'
                });
                view = new View({
                    model: model
                });
                context = view.templateContext();
                expect(context.a).to.equal('b');
                expect(context.foo).to.equal('bar');
                expect(context.model).to.equal(model);
            });
        });

        describe('link handling', function() {
            var TestView = View.extend({
                render: function() {
                    return this.$el.html('<a class=\"internal-link-with-history\" href=\"one\">Internal with default history loading</a>\n<a class=\"internal-link-without-history\" data-no-history=\"true\" href=\"two\">Internal without default history loading</a>\n<a class=\"external-link\" href=\"http://google.com\">External Link</a>');
                }
            });
            var view;

            beforeEach(function() {
                view = new TestView();
                sinon.spy(Backbone.history, 'navigate');
                sinon.spy(window, 'open');
            });

            afterEach(function() {
                $(document).off();
                Backbone.history.navigate.restore();
                window.open.restore();
            });

            it('opens a standard internal link with backbone history', function() {
                view.render();
                view.$('a.internal-link-with-history').click();
                expect(Backbone.history.navigate.firstCall.args[0]).to.match(/^one/);
                expect(Backbone.history.navigate.firstCall.args[1]).to.be.true;
            });

            it('does not open a no-history internal link with backbone history', function() {
                view.render();
                view.$('a.internal-link-without-history').click();
                expect(Backbone.history.navigate.called).to.be.false;
            });

            it('does not open a no-history external link with backbone history', function() {
                view.render();
                $(document).one('click', function(e) {
                    return e.preventDefault();
                });
                view.$('a.external-link').click();
                expect(Backbone.history.navigate.called).to.be.false;
            });

            it('opens an external link in a new tab', function() {
                view.render();
                view.$('a.external-link').click();
                expect(window.open.called).to.be.true;
            });
        });
    });
});
