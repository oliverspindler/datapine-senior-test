/* global define, describe, it, expect, afterEach */
/* jshint expr: true */

define([
    'd3view',
    'd3'
], function (D3View, d3){
    'use strict';

    describe('D3View', function () {
        it('keeps a reference to d3', function () {
            var view = new D3View();
            expect(view.d3).to.equal(d3);
        });

        describe('render', function () {

            afterEach(function () {
                $('.d3view').remove();
                $('style.d3viewtest').remove();
            });

            it('sets thumbnail class when thumbnail mode is active', function () {
                var view = new D3View({ thumbnail: true });
                view.render();
                expect(view.$el.hasClass('thumbnail')).to.be.true;
                expect(view.$el.hasClass('fullsize')).to.be.false;
            });

            it('sets fullsize class when thumbnail mode is not active', function () {
                var view = new D3View();
                view.render();
                expect(view.$el.hasClass('thumbnail')).to.be.false;
                expect(view.$el.hasClass('fullsize')).to.be.true;
            });

            it('derives margins from inner element', function () {
                var view = new D3View({
                    className: 'd3view'
                });
                var style = $('<style class="d3viewtest">.d3view { width: 400px; height: 300px; position: relative; } .d3view .inner { position: absolute; top: 10px; right: 20px; bottom: 30px; left: 40px}>');
                $('body').append(view.$el);
                $('body').append(style);
                view.render();
                expect(view.margin.top).to.equal(10);
                expect(view.margin.right).to.equal(20);
                expect(view.margin.bottom).to.equal(30);
                expect(view.margin.left).to.equal(40);
                expect(view.width).to.equal(340);
                expect(view.height).to.equal(260);
            });
        });
    });
});
