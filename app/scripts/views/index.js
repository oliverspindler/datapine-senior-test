/* global define */

define([
    'underscore',
    'backbone',
    'view',
    'views/population_pyramid',
    'views/stacked_to_grouped',
    'views/cluster_dendrogram',
    'views/bubble_chart'
], function (_, Backbone, View, PopulationPyramidView, StackedToGroupedView, ClusterDendogramView, BubbleChartView, template) {
    'use strict';

    var IndexLayoutView = View.extend({

        template: template,

        render: function () {
            View.prototype.render.apply(this, arguments);
            _.each(this.views, _.bind(function (View) {
                var el = $('<a></a>');
                var view = new View({ thumbnail: true, el: el });
                el.prop('href', view.className);
                el.addClass(view.className);
                this.$el.append(el);
                view.render();
            }, this));
        },

        views: [
            PopulationPyramidView,
            StackedToGroupedView,
            ClusterDendogramView,
            BubbleChartView
        ]
    });

    return IndexLayoutView;
});
