/* global define */

define([
    'd3view',
    'tpl!bubble_chart'
], function (D3View, template) {
    'use strict';

    var BubbleChartView = D3View.extend({

        template: template,

        className: 'bubble_chart',

        events: {
            'change input': 'changeColorScheme'
        },

        render: function () {
            // based on http://bl.ocks.org/mbostock/4063269
            D3View.prototype.render.apply(this, arguments);

            var d3 = this.d3,
                width = this.width,
                height = this.height;

            var format = d3.format(',d'),
                color = d3.scale.category20c();

            var bubble = d3.layout.pack()
                .sort(null)
                .size([width, height])
                .padding(1.5);

            var svg = this.svg = d3.select(this.$el[0]).append('svg')
                .attr('width', width)
                .attr('height', height)
                .attr('class', 'bubble');

            d3.json('/fixtures/flare.json', function(error, root) {
                var node = svg.selectAll('.node')
                    .data(bubble.nodes(classes(root))
                    .filter(function(d) { return !d.children; }))
                    .enter().append('g')
                        .attr('class', 'node')
                        .attr('transform', function(d) { return 'translate(' + d.x + ',' + d.y + ')'; });

                node.append('title')
                    .text(function(d) { return d.className + ': ' + format(d.value); });

                node.append('circle')
                    .attr('r', function(d) { return d.r; })
                    .style('fill', function(d) { return color(d.packageName); });

                node.append('text')
                    .attr('dy', '.3em')
                    .style('text-anchor', 'middle')
                    .text(function(d) { return d.className.substring(0, d.r / 3); });

            });

            // Returns a flattened hierarchy containing all leaf nodes under the root.
            function classes(root) {
                var _classes = [];

                function recurse(name, node) {
                    if (node.children) {
                        node.children.forEach(function(child) { recurse(node.name, child); });
                    }
                    else {
                        _classes.push({packageName: name, className: node.name, value: node.size});
                    }
                }

                recurse(null, root);
                return {children: _classes};
            }

        },

        changeColorScheme: function (e) {
            var d3 = this.d3;
            var colorScale = {
                a: d3.scale.category20c,
                b: d3.scale.category20,
                c: d3.scale.category20b
            }[$(e.target).val()];
            var color = colorScale();
            this.svg.selectAll('circle')
                .style('fill', function(d) { return color(d.packageName); });
        }
    });

    return BubbleChartView;
});
